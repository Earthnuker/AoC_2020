use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| l.trim().parse())
        .collect::<Result<Vec<usize>, _>>()
        .unwrap()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[usize]) -> usize {
    let mut input = input.to_vec();
    input.sort_unstable();
    for (i1, v1) in input.iter().enumerate() {
        if v1 > &2020 {
            break;
        }
        for v2 in input.iter().skip(i1 + 1) {
            if (v1 + v2) == 2020 {
                return v1 * v2;
            }
        }
    }
    panic!("No pair summed to 20202!");
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[usize]) -> usize {
    let mut input = input.to_vec();
    input.sort_unstable();
    for (i1, v1) in input.iter().enumerate() {
        if v1 > &2020 {
            break;
        }
        for (i2, v2) in input.iter().enumerate().skip(i1 + 1) {
            if (v1 + v2) > 2020 {
                break;
            }
            for v3 in input.iter().skip(i2 + 1) {
                if (v1 + v2 + v3) == 2020 {
                    return v1 * v2 * v3;
                }
            }
        }
    }
    panic!("No triplet summed to 20202!");
}
