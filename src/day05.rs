use aoc_runner_derive::{aoc, aoc_generator};

#[derive(Debug, Eq, PartialEq)]
pub enum Direction {
    Front,
    Back,
    Left,
    Right,
}

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vec<Vec<Direction>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    'L' => Direction::Left,
                    'R' => Direction::Right,
                    'F' => Direction::Front,
                    'B' => Direction::Back,
                    other => panic!("Invalid direction: {}", other),
                })
                .collect()
        })
        .collect()
}

pub fn seat_to_pos(seat: &[Direction]) -> (usize, usize) {
    let mut col_range: (usize, usize) = (0, 8);
    let mut row_range: (usize, usize) = (0, 128);
    for inst in seat {
        let delta_col = (col_range.1 - col_range.0) / 2;
        let delta_row = (row_range.1 - row_range.0) / 2;
        match inst {
            Direction::Back => {
                row_range.0 += delta_row;
            }
            Direction::Front => {
                row_range.1 -= delta_row;
            }
            Direction::Left => {
                col_range.1 -= delta_col;
            }
            Direction::Right => {
                col_range.0 += delta_col;
            }
        }
    }
    if row_range.1 - row_range.0 != 1 {
        panic!("Invalid!");
    }
    if col_range.1 - col_range.0 != 1 {
        panic!("Invalid!");
    }
    return (col_range.0, row_range.0);
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &[Vec<Direction>]) -> usize {
    let mut max_seat_id: usize = 0;
    for seat in input {
        let pos = seat_to_pos(seat);
        let seat_id = pos.0 + pos.1 * 8;
        max_seat_id = std::cmp::max(max_seat_id, seat_id);
    }
    return max_seat_id;
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &[Vec<Direction>]) -> usize {
    let mut seats = [[false; 8]; 128];
    for seat in input {
        let pos = seat_to_pos(seat);
        seats[pos.1][pos.0] = true;
    }
    for id in 0..(8 * 128) {
        let mut id = id;
        let mut v = (false, false, false);
        v.0 = seats[id / 8][id % 8];
        id += 1;
        v.1 = seats[id / 8][id % 8];
        id += 1;
        v.2 = seats[id / 8][id % 8];
        if v == (true, false, true) {
            return id - 1;
        }
    }
    panic!("Something went wrong!");
}
