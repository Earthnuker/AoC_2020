use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day9)]
#[inline(always)]
pub fn input_generator(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| l.trim().parse())
        .collect::<Result<Vec<usize>, _>>()
        .unwrap()
}

#[aoc(day9, part1)]
#[inline(always)]
pub fn solve_part1(input: &[usize]) -> usize {
    for (n, win) in input.windows(25).enumerate() {
        let n = n + 25;
        if n >= input.len() {
            continue;
        }
        let c = input[n];
        let mut valid = false;
        for (i1, n1) in win.iter().enumerate() {
            for (i2, n2) in win.iter().enumerate() {
                if i1 == i2 {
                    continue;
                }
                if n1 + n2 == c {
                    valid = true;
                }
            }
        }
        if !valid {
            return c;
        }
    }
    panic!("No match found!");
}

#[aoc(day9, part2)]
#[inline(always)]
pub fn solve_part2_fast(input: &[usize]) -> usize {
    let target_num = solve_part1(input);
    for i in 2..=input.len() {
        if let Some(w) = input.windows(i).find(|w| target_num == w.iter().sum()) {
            return w.iter().max().unwrap() + w.iter().min().unwrap();
        }
    }
    panic!("No match found!");
}
