use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;
#[aoc_generator(day6)]
pub fn input_generator(input: &str) -> Vec<Vec<HashSet<char>>> {
    input
        .split("\n\n")
        .map(|block| {
            block
                .split('\n')
                .map(|line| line.chars().collect())
                .collect()
        })
        .collect()
}

#[aoc(day6, part1)]
fn solve_part1(data: &[Vec<HashSet<char>>]) -> usize {
    let mut total = 0;
    for group in data {
        let mut answers: HashSet<char> = HashSet::new();
        for person in group {
            answers.extend(person);
        }
        total += answers.len();
    }
    return total;
}

#[aoc(day6, part2)]
fn solve_part2(data: &[Vec<HashSet<char>>]) -> usize {
    let mut total = 0;
    for group in data {
        let mut answers: HashSet<char> = HashSet::new();
        for person in group {
            answers.extend(person);
        }
        for person in group {
            answers = answers.intersection(&person).copied().collect();
        }
        total += answers.len();
    }
    return total;
}
