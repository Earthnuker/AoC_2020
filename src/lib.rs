#![allow(clippy::clippy::needless_return)]
use aoc_runner_derive::aoc_lib;
pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
pub mod day08;
pub mod day09;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17p1;
pub mod day17p2;
pub mod day18;
// pub mod day18p2;
aoc_lib! { year = 2020 }
