use aoc_runner_derive::{aoc, aoc_generator};
pub struct Forest {
    slice: Vec<Vec<bool>>,
}

impl Forest {
    fn get(&self, x: usize, y: usize) -> Option<bool> {
        self.slice
            .get(y)
            .map(|line| line.get(x % line.len()))
            .flatten()
            .cloned()
    }
}

#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Forest {
    let slice = input
        .lines()
        .map(|l| l.chars().map(|c| c == '#').collect())
        .collect();
    Forest { slice }
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &Forest) -> usize {
    let mut sum: usize = 0;
    let dx = 3;
    let dy = 1;
    let mut pos = (0, 0);
    while let Some(tree) = input.get(pos.0, pos.1) {
        if tree {
            sum += 1;
        }
        pos.0 += dx;
        pos.1 += dy;
    }
    sum
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &Forest) -> usize {
    let mut prod: usize = 1;
    for (dx, dy) in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        let mut sum: usize = 0;
        let mut pos = (0, 0);
        while let Some(tree) = input.get(pos.0, pos.1) {
            if tree {
                sum += 1;
            }
            pos.0 += dx;
            pos.1 += dy;
        }
        prod *= sum;
    }
    prod
}
